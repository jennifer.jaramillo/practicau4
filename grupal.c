
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <mpi.h>

/*
Integrantes:
Jose RIos
Jennifer Jaramillo
*/

int main(){
    int n,num2, validar = 0, i1,j1,i2,j2, contador=0;
    int proc,t;
    //iniciamos mpi
    MPI_Init(NULL, NULL);
    //rango de procesadores
    MPI_Comm_rank(MPI_COMM_WORLD, &proc);
    //tamaño de procesadores
    MPI_Comm_size(MPI_COMM_WORLD, &t);
    srand(time(NULL));

    if(proc==0){
        //ingresar el tamaño de la matriz
        printf("Ingrese el valor de tamaño de la matriz: \n");
        scanf("%d", &n);
        validar = 1;
        for (int i = 0; i < t; i++){
            MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
    }
    //division de la matriz
    MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    if(n % 2!=0){
        num2 = (int) (n/2) +1;
        n = num2*2;
    }else{
        num2=n/2;
    }
    int m[n][n], m2[n][n];
    int recorrido[n];
    //llenado de matriz
    if(proc==0){
        for (int i = 0; i < num2; i++){
            for (int j = 0; j < n; j++){
                if (i==j){
                    m[i][j]=0;
                }else{
                    //generar nuemeros aleatorios no mayores a 100
                    int a = rand()%100;
                    //cambiar por 100000 cuando el numero aletario creado sea mayor o igual a noventa
                    //o menor o igual a 100
                    if (a >= 90 && a <= 100){
                        m[i][j]=100000;
                    }else{
                        m[i][j]=a;
                    }
                }
            }
            
        }
        MPI_Send(&m, (n*n), MPI_INT, 2, 0, MPI_COMM_WORLD);
    }
    //llenado de matriz
    if(proc==3){
        for (int i = num2; i < n; i++){
            for (int j = 0; j < n; j++){
                if (i==j){
                    m2[i][j]=0;
                }else{
                    int a = rand()%100;
                    //cambiar por 100000 cuando el numero aletario creado sea mayor o igual a noventa
                    //o menor o igual a 100
                    if (a >= 90 && a <= 100){
                        m2[i][j]=100000;
                    }else{
                        m2[i][j]=a;
                    }
                }
            }
        }
        MPI_Send(&m2, (n*n),MPI_INT, 2, 0, MPI_COMM_WORLD);
        
    }

    if(proc==2){//unnion de la matriz
        MPI_Recv(&m, (n*n), MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&m2, (n*n), MPI_INT, 3, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        
        for (int x=num2; x < n; x++){
            for (int y=0; y < n; y++){
                m[x][y] = m2[x][y];
            }
        }

        
        MPI_Send(&m, (n*n), MPI_INT, 4, 0, MPI_COMM_WORLD);
        printf("pasa 1[%d]\n",proc);
    }

    if(proc==4){//present de la 1ra mitad de la matr
        MPI_Recv(&m, (n*n), MPI_INT, 2, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Presentar Matriz\n");
        for (int x=0; x < num2; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",m[x][y]);
            }
            printf("\n");
        }

        MPI_Send(&m, (n*n), MPI_INT, 5, 0, MPI_COMM_WORLD);
    }

    if(proc==5){//present
        MPI_Recv(&m, (n*n), MPI_INT, 4, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        sleep(2);
        for (int x=n/2; x < n; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",m[x][y]);
            }
            printf("\n");
        }
        MPI_Send(&m, (n*n), MPI_INT, 1, 0, MPI_COMM_WORLD);
    }


    if(proc==1){//se actualiza la matriz
        MPI_Recv(&m, (n*n), MPI_INT, 5, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        for (int k = 0; k < n; k++){
            for (int i = 0; i < n; i++){
                for (int j = 0; j < n; j++){
                    int dt = m[i][k];
                    if (m[i][j] == 100000){
                        m[i][j] = dt;
                    }
                }
            }
        }
        MPI_Send(&m, (n*n), MPI_INT, 2, 0, MPI_COMM_WORLD);
        MPI_Send(&m, (n*n), MPI_INT, 3, 0, MPI_COMM_WORLD);
        
    }
    if(proc==2){
        sleep(5);//se presenta la nueva matriz
        MPI_Recv(&m, (n*n), MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("se organiza la matriz\n");
        for (int x=0; x < num2; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",m[x][y]);
            }
            printf("\n");
        }
    }

    if(proc==3){//present mitad
        MPI_Recv(&m, (n*n), MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        sleep(2);
        for (int x=n/2; x < n; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",m[x][y]);
            }
            printf("\n");
        }
        MPI_Send(&m, (n*n), MPI_INT, 0, 0, MPI_COMM_WORLD);
    }


    if(proc==0){//se busca el camino más cort
        sleep(2);
        MPI_Recv(&m, (n*n), MPI_INT, 3, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        validar=0;
        while (validar==0)
        {
            printf("Ingrese la ruta origen: \n");
            scanf("%d %d", &i1,&j1);
            printf("Ingrese la ruta destino: \n");
            scanf("%d %d", &i2,&j2);

            if (i1>=0 && i1<=(n-1) && j1>=0 && j1<=(n-1) && j2<n && i2<n ){
            // Se controla siel valor de origen y destino esta en la misma fila  
                if (i1==i2){
                    int val = 0;
                    int i = i1, j= j1;
                    if(j1<j2){
                        while (val==0){
                            recorrido[contador] = m[i][j];
                            if(j==j2)
                                val=1;
                            j++;
                            contador++;
                        }
                        //lado izquierdo
                    }else if(j1>j2){
                        while (val==0){
                            recorrido[contador] = m[i][j];
                            if(j==j2)
                                val=1;
                            j--;
                            contador++;
                        }
                    }
                // Se controla siel valor de origen y destino esta en la misma columna
                }else if (j1==j2){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        recorrido[contador] = m[i][j];
                        if(i==i2)
                            val=1;
                        i++;
                        contador++;
                    } 
                    //Diagonal S
                }else if (i1==j1 && i2==j2){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        recorrido[contador] = m[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        contador++;
                    }
                    //Diagonal P
                }else if(i1<j1 && i2<j2 && (j1-i1)==(j2-i2)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        recorrido[contador] = m[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        contador++;
                    }
                    //B diagonal P
                }else if(i1>j1 && i2>j2 && (i1-j1)==(i2-j2)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        recorrido[contador] = m[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        contador++;
                    }
                    //Diagonal S
                }else if((i1+j1) == (n-1) && (i2+j2)== (n-1)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        recorrido[contador] = m[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j--;
                        contador++;
                    }
                    //SB Diagonal S
                }else if((i1+j1)==(i2+j2)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        recorrido[contador] = m[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j--;
                        contador++;
                    }
                    //otras posiciones
                }else{
                    int i = i1, j= j1;
                    if (j<j2){
                        int val =0;
                        while (val==0){
                            recorrido[contador] = m[i][j];
                            if(i==i2){
                                if(j<j2){ 
                                    int val2 = 0;
                                    while (val2==0){
                                        recorrido[contador] = m[i][j];
                                        if(i==i2 && j==j2){
                                            val2=1;
                                            val = 1;
                                        }
                                        contador++;
                                        j++;
                                    }
                                }else if(j>j2){
                                    int val2 = 0;
                                    while (val2==0){
                                        recorrido[contador] = m[i][j];
                                        if(i==i2 && j==j2){
                                            val2= 1;
                                            val = 1;
                                        }
                                        contador++;
                                        j--;
                                    }
                                }
                            }else if(j==j2){
                                int val2 = 0;
                                while (val2==0){
                                    recorrido[contador] = m[i][j];
                                    if(i==i2 && j==j2){
                                        val2= 1;
                                        val = 1;
                                    }
                                    contador++;
                                    i++;
                                }
                            }else{
                                contador++;
                                i++;
                                j++;
                            }
                            
                        }
                    }else if(j>j2){
                        int val =0;
                        while (val==0){
                            recorrido[contador] = m[i][j];
                            if(i==i2){
                                if(j<j2){ 
                                    int val2 = 0;
                                    while (val2==0){
                                        recorrido[contador] = m[i][j];
                                        if(i==i2 && j==j2){
                                            val2=1;
                                            val = 1;
                                        }
                                        contador++;
                                        j++;
                                    }
                                }else if(j>j2){
                                    int val2 = 0;
                                    while (val2==0){
                                        recorrido[contador] = m[i][j];
                                        if(i==i2 && j==j2){
                                            val2= 1;
                                            val = 1;
                                        }
                                        contador++;
                                        j--;
                                    }
                                }
                            }else if(j==j2){
                                int val2 = 0;
                                while (val2==0){
                                    recorrido[contador] = m[i][j];
                                    if(i==i2 && j==j2){
                                        val2= 1;
                                        val = 1;
                                    }
                                    contador++;
                                    i++;
                                }
                            }else{
                                contador++;
                                i++;
                                j--;
                            }
                            
                        }
                    }
                }
                printf("Origen: %d  Destino: %d  ruta: ",m[i1][j1], m[i2][j2]);
                for (int i = 0; i < contador; i++){
                    printf(" [%d] ", recorrido[i]);
                }
                printf("\n");   

                validar = 1;
            }else{
                printf("INgrese rutas correctas\n");
            }

        }
    }

    MPI_Finalize();
    return 0;
}